# Field Maker

This module is a Drush utility to add fields.

Run the following command to get started: 
> drush fmc [entity_type] [bundle]
