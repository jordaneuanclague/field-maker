<?php
/**
 * @file Module file for field_maker.
 */

/**
 * Implements hook_drush_command().
 */
function field_maker_drush_command() {
  $items['field-maker-create'] = [
    'description' => 'Create fields',
    'aliases' => ['fmc'],
    'arguments' => ['entity', 'bundle'],
  ];

  return $items;
}

/**
 * Callback from hook_drush_command().
 */
function drush_field_maker_create($entity = '', $bundle = '') {
  $entity_info = entity_get_info();
  $entities = array_keys($entity_info);

  // Entity.
  if (empty($entity)) {
    $entity = $entities[drush_choice($entities, dt('Which ENTITY do you want to add fields to?'))];
  }
  else {
    if (!in_array($entity, $entities)) {
      drush_log('Supplied entity is invalid', 'error');
      die();
    }
  }

  // Bundles.
  $bundles = array_keys($entity_info[$entity]['bundles']);
  if (empty($bundle)) {
    $bundle = $bundles[drush_choice($bundles, dt('Which BUNDLE do you want to add fields to?'))];
  }
  else {
    if (!in_array($bundle, $bundles)) {
      drush_log('Supplied bundle is invalid', 'error');
      die();
    }
  }

  // Get machine names for fields to add.
  $fields = explode(' ', drush_prompt('Enter the machine names of the field you want to add'));

  // Set up data structure.
  $fields = array_flip($fields);
  $fields = array_map(function($field) {
    return [];
  }, $fields);

  // Set field's cardinality and type.
  $supported_types = field_maker_get_supported_field_types();
  foreach ($fields as $field => $data) {
    $cardinality = drush_prompt(dt(
      '@field: What is the cardinality for this field? (For unlimited, enter -1)',
      ['@field' => $field]
    ));
    $fields[$field]['cardinality'] = $cardinality;

    $type = drush_choice($supported_types, dt(
      '@field: What is this field\'s type?',
      ['@field' => $field]
    ));
    $fields[$field]['type'] = $supported_types[$type];
  }

  // Create the fields.
  field_maker_create_fields($entity, $bundle, $fields);
}

/**
 * Helper function to create our fields, and bind them to the instance.
 */
function field_maker_create_fields($entity, $bundle, $fields) {
  $created = ['fields' => [], 'instances' => []];

  foreach ($fields as $field => $field_data) {
    $add_field_instance = TRUE;

    // Create the field.
    if (!field_info_field($field)) {
      try {
        field_create_field([
          'field_name' => $field,
          'type' => $field_data['type'],
          'entity_types' => [],
          'cardinality' => $field_data['cardinality'] > 0
            ? $field_data['cardinality']
            : FIELD_CARDINALITY_UNLIMITED,
          'module' => 'field_maker',
        ]);

        drush_log(dt(
          '@field: Field successfully created',
          ['@field' => $field]
        ), 'success');

        $created['fields'][] = $field;
      } catch (FieldException $e) {
        drush_log(dt(
          '@field: Field could not be created',
          ['@field' => $field]
        ), 'error');

        field_maker_cleanup($created);
      }
    }
    else {
      drush_log(dt(
        '@field: Field already exists',
        ['@field' => $field]
      ), 'warning');

      $add_field_instance = drush_confirm(dt(
        'Do you still want to try and attach this field to the @entity:@bundle?',
        ['@entity' => $entity, '@bundle' => $bundle]
      ));
    }

    // Create the instance.
    if ($add_field_instance) {
      if (!field_info_instance($entity, $field, $bundle)) {
        try {
          $i = field_create_instance([
            'field_name' => $field,
            'entity_type' => $entity,
            'bundle' => $bundle,
            'label' => 'Field Maker: ' . ucfirst(str_replace('_', ' ', $field)),
          ]);

          drush_log(dt(
            '@field: Field instance successfully created on @entity:@bundle',
            ['@field' => $field, '@entity' => $entity, '@bundle' => $bundle]
          ), 'success');

          $created['instances'][] = $i;
        } catch (FieldException $e) {
          drush_log(dt(
            '@field: Field instance could not be created on @entity:@bundle',
            ['@field' => $field, '@entity' => $entity, '@bundle' => $bundle]
          ), 'error');

          field_maker_cleanup($created);
        }
      }
      else {
        drush_log(dt(
          '@field: Field instance already exists on @entity:@bundle',
          ['@field' => $field, '@entity' => $entity, '@bundle' => $bundle]
        ), 'warning');
      }
    }
  }
}

/**
 * Helper function.
 */
function field_maker_get_supported_field_types() {
  return ['text', 'text_long', 'text_with_summary'];
}

/**
 * Cleanup created fields/instances.
 */
function field_maker_cleanup($created) {
  drush_log(dt('Performing cleanup...'), 'warning');

  foreach ($created['instances'] as $instance) {
    field_delete_instance($instance, FALSE);
    drush_log(dt(
      '@field: Field instance successfully deleted on @entity:@bundle',
      ['@field' => $instance['field_name'], '@entity' => $instance['entity_type'], '@bundle' => $instance['bundle']]
    ), 'success');
  }
  foreach ($created['fields'] as $field) {
    if (field_info_field($field) != NULL) {
      field_delete_field($field);
      drush_log(dt(
        '@field: Field successfully deleted',
        ['@field' => $field]
      ), 'success');
    }
  }

  drush_log(dt('Cleanup complete!'), 'success');
  die();
}
